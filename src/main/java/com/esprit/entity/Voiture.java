package com.esprit.entity;
import javax.persistence.Id;
import javax.persistence.Entity;
@Entity
public class Voiture {
    @Id
    public  int Immatriculation;

    public Voiture(int immatriculation) {
        Immatriculation = immatriculation;
    }

    public int getImmatriculation() {
        return Immatriculation;
    }

    public void setImmatriculation(int immatriculation) {
        Immatriculation = immatriculation;
    }
}
